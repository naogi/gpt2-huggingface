import random

from transformers import pipeline, set_seed
from naogi import NaogiModel

class Model(NaogiModel):
  def init_model(self):
    self.model = pipeline('text-generation', model='gpt2')

  def warming_up(self):
    pipeline('text-generation', model='gpt2')

  def load_model(self):
    pass

  def predict(self):
    return self.model(
      self.text,
      max_length = self.max_len,
      num_return_sequences = self.max_seq
    )

  def prepare(self, params):
    def safe_int(val, default):
      try:
        return int(val)
      except (ValueError, TypeError, KeyError):
        return default

    rseed = int(random.random() * 1000)
    seed = safe_int(params['seed'], rseed) if ('seed' in params) else rseed
    set_seed(seed)

    self.text = params['text']
    self.max_seq = safe_int(params['max_seq'], 1) if ('max_seq' in params) else 1
    self.max_len = safe_int(params['max_length'], 50) if ('max_length' in params) else 50
