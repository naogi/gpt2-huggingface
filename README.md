### gpt2-huggingface API

API returns JSON array (with `max_seq` items) with GPT2 generated texts

```shell
curl -X GET '<naogi_project_url>/predict?text=Hello%20my%20friend&max_seq=3&max_length=50'
```
